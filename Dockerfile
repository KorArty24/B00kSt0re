#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mongo
COPY init.json /init.json mongo-seed
CMD mongoimport --host mongodb --db reach-engine --collection users --authenticationDatabase admin --username user --password P@ssw0rd --type json --file /init.json --jsonArray

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["BookStore.csproj", "."]
RUN dotnet restore "BookStore.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "BookStore.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "BookStore.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "BookStore.dll"]


