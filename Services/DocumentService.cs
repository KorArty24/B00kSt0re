﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using BookStore.Models;

namespace BookStore.Services

{
    public class DocumentService
    {
        private readonly IMongoCollection<Document> _documents;

        public DocumentService(IDocumentsDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionStrings);

            var database = client.GetDatabase(settings.DatabaseName);

            _documents = database.GetCollection<Document>(settings.DocumentsCollectionName);


        }

        public List<Document> Get() => _documents.Find(book => true).ToList();

        public Document Get(string id) => _documents.Find(document => document.Id == id).FirstOrDefault();

        public List<string> GetMostProductiveWriters()
        {
            var docs = _documents.Aggregate().Group(u => u.AuthorName, val => new { AuthorName = val.Key, total = val.Sum(u => 1) }).SortByDescending(x => x.total).Limit(10).ToList();
            List<string> lst = new List<string>();
            foreach (var dc in docs)
            {
                lst.Add(dc.ToString());
            }
            return lst;
        }
    }



}

