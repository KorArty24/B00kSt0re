﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Models
{
    public class DocumentsDatabaseSettings: IDocumentsDatabaseSettings
    {
        public string DocumentsCollectionName { get; set; }
        public string ConnectionStrings { get; set; }
        public string DatabaseName { get; set; }

    }

    public interface IDocumentsDatabaseSettings
    {
        string DocumentsCollectionName { get; set; }
        string ConnectionStrings { get; set; }
        string DatabaseName { get; set; }
    }
}
