﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookStore.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using BookStore.Services;

namespace BookStore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentsController
    {
        private readonly DocumentService _service;
        public DocumentsController(DocumentService dservice)
        {
            _service = dservice;
        }

        [HttpGet]
        public ActionResult<List<string>> getAuthors() 
        {
            var result = _service.GetMostProductiveWriters();
            return result;
        }
    }
}
